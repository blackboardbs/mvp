#!/usr/bin/env bash

# Put the app into maintenance mode
php artisan down

dt=`date '+%d-%m-%Y %H:%M:%S'`

# Run mysql backup
#php artisan backup:run

# I then zip the main folders content except the.env,public,.git,.idea
#zip -r app-$dt.zip . -x ".env" ".git"

# I then zip the public folder except the htaccess, and index.php files
#zip -r public-$dt.zip public -x "public/.htaccess" "public/index.php"

# Pull from the git
git fetch --all

git reset --hard origin/development
#git reset --hard origin/feature/resource-server

git pull origin development
#git pull origin feature/resource-server
#sudo git clone git@bitbucket.org:blackboardbs/attooh.git

# Install composer dependencies
composer install --no-interaction --prefer-dist --optimize-autoloader --no-dev

# Clear caches
php artisan cache:clear
php artisan route:clear
php artisan config:clear

# Generate caches
#php artisan route:cache
php artisan config:cache


npm install
# Install node dependencies
npm ci

# Build production javascript
npm run prod

# I then have to set the permissions of the folder vendor/h4cc to 777
#chmod 777 vendor/h4cc
#chmod 777 public/tmp
chmod -R 777 storage

# And edit the path in the snappy config file.

# Put the app out of maintenance mode
php artisan up